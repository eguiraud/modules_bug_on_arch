FROM archlinux

COPY packages packages
RUN pacman -Syu --noconfirm --quiet $(cat packages)
RUN git clone https://github.com/root-project/root.git root_src &&\
    mkdir build_root &&\
    cmake -DCMAKE_BUILD_TYPE=Debug -Dtesting=OFF -Droottest=OFF -Droofit=OFF -Dtmva=OFF -Bbuild_root -Sroot_src
RUN cmake --build build_root -- -j4
COPY repro_modules_crash.C repro_modules_crash.C
